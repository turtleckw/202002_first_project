# Kaggler競賽 - Predict Future Sales

### 設定顯示最大列數與行數
    import numpy as np
    import pandas as pd
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 100)
## 資料清理使用套件
    from itertools import product
    from sklearn.preprocessing import LabelEncoder
## 繪製圖表使用套件
    import seaborn as sns
    import matplotlib.pyplot as plt
    %matplotlib inline
## xgboost模型使用套件
    from xgboost import XGBRegressor
    from xgboost import plot_importance

## 系統設置使用套件
    import time
    import sys
    import gc
    import pickle
    sys.version_info

## 模型欄位重要性視覺化
    def plot_features(booster, figsize):    
        fig, ax = plt.subplots(1,1,figsize=figsize)
        return plot_importance(booster=booster, ax=ax)

## 讀取各資料集 
    items = pd.read_csv('../input/items.csv')
    shops = pd.read_csv('../input/shops.csv')
    cats = pd.read_csv('../input/item_categories.csv')
    train = pd.read_csv('../input/sales_train.csv')
## 將測試資料的ID欄位作為index
    test  = pd.read_csv('../input/test.csv').set_index('ID')
